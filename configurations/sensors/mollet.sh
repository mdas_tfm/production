#!/bin/bash

export SENSOR_NAMESPACE="tfm"
export SENSOR_AREA_NAME="Mollet"
export SENSOR_GW_ADDRESS="gateway.tfm.svc.cluster.local"
export LOCATION="41.539,2.213"
export SENSOR_INTERVAL="15s"
export SENSOR_METRICS_PORT="2112"
export SENSOR_NAME="sensor-mollet"
