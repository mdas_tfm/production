#!/bin/bash

set -euo pipefail
# set -x

KAFKA_NAMESPACE="streams"
ES_NAMESPACE="elasticsearch"
TFM_NAMESPACE="tfm"
KAFKA_BOOTSTRAP_SVC="streams-cluster-kafka-bootstrap"
ES_HTTP_SVC="meteo-lt-cluster-es-http"
KUBECTL="$(which kubectl)"
SENSORS="assets/sensor/"
STPROC="assets/short_term_processor/kubernetes.json"
LTPROC="assets/long_term_processor/kubernetes.json"
GATEWAY="assets/gateway/kubernetes.json"
GATEWAY_DEPL_NAME="gateway"
LT_DEPL_NAME="long-term-processor"
ST_DEPL_NAME="st-processor"
SENSOR_CONFIGS="configurations/sensors"


print_info() {
    printf "\e[36m[INFO] ==> %s \e[0m\n" "${1:-'unknown'}"
}

print_ok() {
    printf "\e[32m[SUCCESS] ==> %s \e[0m\n" "${1:-'unknown'}"
}

print_error() {
    printf "==> \e[31m[ERROR] ==> failed %s \e[0m\n" "${1:-'unknown'}"
    exit 1
}

prereqs() {
    if [ -z "${KUBECONFIG}" ]; then
        print_error "Missing KUBECONFIG var"
    fi
    if ! kubectl get svc -n ${KAFKA_NAMESPACE} | grep "${KAFKA_BOOTSTRAP_SVC}" > /dev/null 2>&1; then
        print_error "Kafka cluster is missing"
    fi
    if ! kubectl get svc -n ${ES_NAMESPACE} | grep "${ES_HTTP_SVC}" > /dev/null 2>&1; then
        print_error "ElasticSearch cluster is missing"
    fi
}

create_ns() {
    ${KUBECTL} create ns ${TFM_NAMESPACE} || true
}

kctl_rollout_wait() {
    DEPLOYMENT="${1}"
    ATTEMPTS=0
    ROLLOUT_STATUS_CMD="${KUBECTL} rollout status deployment/${DEPLOYMENT} --timeout=1m -n ${TFM_NAMESPACE}"
    until $ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq 10 ]; do
        ATTEMPTS=$((ATTEMPTS + 1))
    done

    if [ $ATTEMPTS -ge 10 ]; then
        print_info "deployment for ${DEPLOYMENT} failed, rolling it back ..."
        PREVTAG="$(git tag | tail -2 | head -1)"
        git checkout "${PREVTAG}"
        kctl_apply "${SENSORS}kubernetes-${DEPLOYMENT}.json"
        sleep 10 && ${ROLLOUT_STATUS_CMD}
        print_error "deployment ${DEPLOYMENT} rolled back"
    fi
}

kctl_wait_deployment() {
    WAITFOR="${1:-none}"
    ${KUBECTL} wait --for=condition=Available deployment/"${WAITFOR}" -n ${TFM_NAMESPACE} --timeout=600s || print_error "waiting for ${WAITFOR}"
}

kctl_apply() {
    ASSETS="${1:-x}"
    if [ "${ASSETS}" == "x" ]; then
        print_error "kctl_apply called without parameters..."
    fi

    ${KUBECTL} apply -f "${ASSETS}" --record -n ${TFM_NAMESPACE} || print_error "applying ${ASSETS} ..."
}

kctl_set_image() {
    DEPLOYMENT="${1}"
    CONTAINER_NAME="${2}"
    CONTAINER_IMAGE="${3}"
    VERSION="${4}"
    kubectl set image deployment/"${DEPLOYMENT}" "${CONTAINER_NAME}"="${CONTAINER_IMAGE}":"${VERSION}" --record -n ${TFM_NAMESPACE}
}

deploy_sensors() {
    VERSION="${1:-x}"
    if [ "${VERSION}" == "x" ]; then
        # initial deployment
        kctl_apply "${SENSORS}"
        for sensor in "${SENSOR_CONFIGS}"/*.sh; do
            # shellcheck disable=SC1090
            source "$sensor"
            kctl_wait_deployment "${SENSOR_NAME}"
        done
    else
        for sensor in "${SENSOR_CONFIGS}"/*.sh; do
            # shellcheck disable=SC1090
            source "$sensor"
            print_info "deploying ${SENSOR_NAME} version ${VERSION}"
            EXISTS="$(${KUBECTL} get deployment "${SENSOR_NAME}" -n ${TFM_NAMESPACE} --ignore-not-found --no-headers | wc -l)"
            kctl_apply "${SENSORS}kubernetes-${SENSOR_NAME}.json"
            if [ "${EXISTS}" -ne 0 ]; then
                kctl_rollout_wait "${SENSOR_NAME}" || print_error "rolling out deployment ${SENSOR_NAME}"
            else
                kctl_wait_deployment "${SENSOR_NAME}" || print_error "waiting for ${SENSOR_NAME} to deploy ..."
            fi
            print_info "deployment ${SENSOR_NAME} rolled out successully"
        done
    fi
}

deploy_gateway() {
    VERSION="${1:-x}"
    if [ "${VERSION}" == "x" ]; then
        # initial deployment
        kctl_apply "${GATEWAY}"
        kctl_wait_deployment "${GATEWAY_DEPL_NAME}"
    else
        print_info "deploying gateway version ${VERSION}"
        EXISTS="$(${KUBECTL} get deployment "${GATEWAY_DEPL_NAME}" -n ${TFM_NAMESPACE} --ignore-not-found --no-headers | wc -l)"
        kctl_apply "${GATEWAY}"
        if [ "${EXISTS}" -ne 0 ]; then
            kctl_rollout_wait "${GATEWAY_DEPL_NAME}" || print_error "rolling out deployment ${GATEWAY_DEPL_NAME}"
        else
            kctl_wait_deployment "${GATEWAY_DEPL_NAME}" || print_error "waiting for ${GATEWAY_DEPL_NAME} to deploy ..."
        fi
        print_info "deployment ${GATEWAY_DEPL_NAME} rolled out successully"
    fi
}

deploy_short_term() {
    VERSION="${1:-x}"
    if [ "${VERSION}" == "x" ]; then
        # initial deployment
        kctl_apply "${STPROC}"
        kctl_wait_deployment "${ST_DEPL_NAME}"
    else
        print_info "deploying short term version ${VERSION}"
        EXISTS="$(${KUBECTL} get deployment "${ST_DEPL_NAME}" -n ${TFM_NAMESPACE} --ignore-not-found --no-headers | wc -l)"
        kctl_apply "${STPROC}"
        if [ "${EXISTS}" -ne 0 ]; then
            kctl_rollout_wait "${ST_DEPL_NAME}" || print_error "rolling out deployment ${ST_DEPL_NAME}"
        else
            kctl_wait_deployment "${ST_DEPL_NAME}" || print_error "waiting for ${ST_DEPL_NAME} to deploy ..."
        fi
        print_info "deployment ${ST_DEPL_NAME} rolled out successully"
    fi
}

deploy_long_term() {
    VERSION="${1:-x}"
    if [ "${VERSION}" == "x" ]; then
        # initial deployment
        kctl_apply "${LTPROC}"
        kctl_wait_deployment "${LT_DEPL_NAME}"
    else
        print_info "deploying long term version ${VERSION}"
        EXISTS="$(${KUBECTL} get deployment "${LT_DEPL_NAME}" -n ${TFM_NAMESPACE} --ignore-not-found --no-headers | wc -l)"
        kctl_apply "${LTPROC}"
        if [ "${EXISTS}" -ne 0 ]; then
            kctl_rollout_wait "${LT_DEPL_NAME}" || print_error "rolling out deployment ${LT_DEPL_NAME}"
        else
            kctl_wait_deployment "${LT_DEPL_NAME}" || print_error "waiting for ${LT_DEPL_NAME} to deploy ..."
        fi
        print_info "deployment ${LT_DEPL_NAME} rolled out successully"
    fi
}

deploy_component() {
    prereqs

    COMPONENT="$(git log -1 --pretty=oneline | awk '{print $(NF)}')"
    VERSION="$(git log -1 --pretty=oneline | awk '{print $(NF-1)}')"
    # shellcheck disable=SC2034
    PATTERN="(\d+\.){2}\d+"

    if [ "x${COMPONENT}" == "x" ]; then
        print_error "no component found"
    fi

    if [ "x${VERSION}" == "x" ]; then
        print_error "no version found"
    fi

    RESULT="${VERSION//PATTERN/}"

    if [ "${RESULT}" != "${VERSION}" ]; then
        print_error "unexpected version ${VERSION}"
    fi

    if [ ! -d "assets/${COMPONENT}" ]; then
        print_error "unknown component ${COMPONENT}"
    fi

    case "${COMPONENT}" in
        "gateway")
            deploy_gateway "${VERSION}"
            ;;
        "long_term_processor")
            deploy_long_term "${VERSION}"
            ;;
        "short_term_processor")
            deploy_short_term "${VERSION}"
            ;;
        "sensor")
            deploy_sensors "${VERSION}"
            ;;
        *)
            print_error "we should not be here"
            ;;
    esac
}

implode() {
    ${KUBECTL} delete ns ${TFM_NAMESPACE} || true
    print_ok "Namespace ${TFM_NAMESPACE} has been deleted"
}

entry_point() {
    OPTION="${1:-full}"

    case "${OPTION}" in
        "full")
            create_ns
            deploy_gateway
            deploy_sensors
            deploy_short_term
            deploy_long_term
            ;;
        "ci")
            deploy_component
            ;;
        "undeploy")
            implode
            ;;
        *)
            print_error "Option ${OPTION} is not supported"
            ;;
    esac
}


if [ $# -ne 1 ]; then
    OPTION="full"
else
    OPTION="$1"
fi

entry_point "${OPTION}"
